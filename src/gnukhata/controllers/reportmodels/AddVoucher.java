/* This file is part of GNUKhata:A modular,robust and Free Accounting System.

  GNUKhata is Free Software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3 of
  the License, or (at your option) any later version.and old.stockflag = 's'

  GNUKhata is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with GNUKhata (COPYING); if not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA  02110-1301  USA59 Temple Place, Suite 330*/

package gnukhata.controllers.reportmodels;

/**
 * @author kk
 *
 */
public class AddVoucher {
private String DrCr;
private String AccountName;
private double DrAmount;
private double CrAmount;
/**
 *@param drCr
 * @param accountName
 * @param drAmount
 * @param crAmount
 */
public AddVoucher(String drCr, String accountName, double drAmount,double crAmount) {
	super();
	DrCr = drCr;
	AccountName = accountName;
	DrAmount = drAmount;
	CrAmount = crAmount;
}
/**
 * @return the drCr
 */
public String getDrCr() {
	return DrCr;
}
/**
 * @return the accountName
 */
public String getAccountName() {
	return AccountName;
}
/**
 * @return the drAmount
 */
public double getDrAmount() {
	return DrAmount;
}
/**
 * @return the crAmount
 */
public double getCrAmount() {
	return CrAmount;
}


}
