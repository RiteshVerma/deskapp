/* This file is part of GNUKhata:A modular,robust and Free Accounting System.

  GNUKhata is Free Software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3 of
  the License, or (at your option) any later version.and old.stockflag = 's'

  GNUKhata is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with GNUKhata (COPYING); if not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA  02110-1301  USA59 Temple Place, Suite 330*/


package gnukhata.controllers.reportmodels;

public class VoucherDetail {
	private String voucherNo;
	private String voucherType;
	private String dateOfTransaction;
	private String drAccount;
	private String crAccount;
	private String amount;
	private String narration;
	private String projectName;
	private String voucherCode;
	private int lockflag;
	public VoucherDetail(String voucherNo, String voucherType,
			String dateOfTransaction, String drAccount, String crAccount,
			String amount, String narration, String voucherCode, String projectName, int lockflag) {
		
		this.voucherNo = voucherNo;
		this.voucherType = voucherType;
		this.dateOfTransaction = dateOfTransaction;
		this.drAccount = drAccount;
		this.crAccount = crAccount;
		this.amount = amount;
		this.narration = narration;
		this.projectName = projectName;
		this.voucherCode = voucherCode;
		this.lockflag = lockflag;
	}

	/**
	 * @return the vouvherNo
	 */
	public String getVoucherNo() {
		return voucherNo;
	}

	/**
	 * @return the voucherCode
	 */
	public String getVoucherCode() {
		return voucherCode;
	}

	/**
	 * @return the voucherType
	 */
	public String getVoucherType() {
		return voucherType;
	}

	/**
	 * @return the dateOfTransaction
	 */
	public String getDateOfTransaction() {
		return dateOfTransaction;
	}

	/**
	 * @return the drAccount
	 */
	public String getDrAccount() {
		return drAccount;
	}

	/**
	 * @return the craccount
	 */
	public String getCrAccount() {
		return crAccount;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @return the narration
	 */
	public String getNarration() {
		return narration;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}
	
	public int getlockFlag(){
		return lockflag;
	}

	/**
	 * @return the lockflag
	 */
	public int getLockflag() {
		return lockflag;
	}

	/**
	 * @param lockflag the lockflag to set
	 */
	public void setLockflag(int lockflag) {
		this.lockflag = lockflag;
	}
	
	

}
