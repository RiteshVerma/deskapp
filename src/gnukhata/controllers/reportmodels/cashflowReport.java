/* This file is part of GNUKhata:A modular,robust and Free Accounting System.

  GNUKhata is Free Software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3 of
  the License, or (at your option) any later version.and old.stockflag = 's'

  GNUKhata is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with GNUKhata (COPYING); if not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA  02110-1301  USA59 Temple Place, Suite 330*/


package gnukhata.controllers.reportmodels;

/**
 * @author inu
 *
 */
public class cashflowReport {
/**
	 * @param accName
	 * @param amounts
	 * @param accName1
	 * @param amounts1
	 */

	private String accName;
	private String amounts;
	private String accName1;
	private String amounts1;
	
	public cashflowReport(String accName, String amounts, String accName1,
			String amounts1) {
		super();
		this.accName = accName;
		this.amounts = amounts;
		this.accName1 = accName1;
		this.amounts1 = amounts1;
	}

	/**
	 * @return the accName
	 */
	public String getAccName() {
		return accName;
	}

	/**
	 * @return the amounts
	 */
	public String getAmounts() {
		return amounts;
	}

	/**
	 * @return the accName1
	 */
	public String getAccName1() {
		return accName1;
	}

	/**
	 * @return the amounts1
	 */
	public String getAmounts1() {
		return amounts1;
	}

}
