/* This file is part of GNUKhata:A modular,robust and Free Accounting System.

  GNUKhata is Free Software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3 of
  the License, or (at your option) any later version.and old.stockflag = 's'

  GNUKhata is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with GNUKhata (COPYING); if not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA  02110-1301  USA59 Temple Place, Suite 330*/


package gnukhata.controllers.reportmodels;

public class extendedTrialBalance {
private String srNo;
private String accountName;
private String groupName;
private String openingBalance;
private String totalDrTransactions;
private String totalCrTransactions;
private String drBalance;
private String crBalance;
//Class definition starts here, it is just a pojo with columns as properties.
//we have getters to actually return the respective value for the columns.
public extendedTrialBalance(String srNo, String accountName, String groupName,
		String openingBalance, String totalDrTransactions,
		String totalCrTransactions, String drBalance, String crBalance) {
	super();
	this.srNo = srNo;
	this.accountName = accountName;
	this.groupName = groupName;
	this.openingBalance = openingBalance;
	this.totalDrTransactions = totalDrTransactions;
	this.totalCrTransactions = totalCrTransactions;
	this.drBalance = drBalance;
	this.crBalance = crBalance;
}
public String getSrNo() {
	return srNo;
}
public String getAccountName() {
	return accountName;
}
public String getGroupName() {
	return groupName;
}
public String getOpeningBalance() {
	return openingBalance;
}
public String getTotalDrTransactions() {
	return totalDrTransactions;
}
public String getTotalCrTransactions() {
	return totalCrTransactions;
}
public String getDrBalance() {
	return drBalance;
}
public String getCrBalance() {
	return crBalance;
}





}
