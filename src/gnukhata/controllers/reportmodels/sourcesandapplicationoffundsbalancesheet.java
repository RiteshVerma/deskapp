/* This file is part of GNUKhata:A modular,robust and Free Accounting System.

  GNUKhata is Free Software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3 of
  the License, or (at your option) any later version.and old.stockflag = 's'

  GNUKhata is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with GNUKhata (COPYING); if not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA  02110-1301  USA59 Temple Place, Suite 330*/

package gnukhata.controllers.reportmodels;

/**
 * @author inu
 *
 */
public class sourcesandapplicationoffundsbalancesheet {
	private String groupName;
	private String amount1;
	private String amount2;
	/**
	 * @param groupName
	 * @param amount1
	 * @param amount2
	 */
	public sourcesandapplicationoffundsbalancesheet(String groupName,
			String amount1, String amount2) {
		super();
		this.groupName = groupName;
		this.amount1 = amount1;
		this.amount2 = amount2;
	}
	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * @return the amount1
	 */
	public String getAmount1() {
		return amount1;
	}
	/**
	 * @return the amount2
	 */
	public String getAmount2() {
		return amount2;
	}
	
	
}
